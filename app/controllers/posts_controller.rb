class PostsController < ApplicationController
  def index
    @posts = Post.all
  end

  def create
    @posts = Post.all
    @post = Post.new(params[:post])
    @post.save

    if request.xhr?
      render partial: 'posts/post', locals: { post: @post }
    else
      redirect_to posts_url
    end
  end

  def show
    @post = Post.find(params[:id])

    if request.xhr?
      render json: @post
    else
      redirect_to posts_url
    end
  end

  def toggle_fav
    @post = Post.find(params[:id])
    @post.toggle!(:fav)

    if request.xhr?
      render json: @post
    else
      redirect_to posts_url
    end
  end
end
